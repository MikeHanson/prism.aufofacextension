# Simple Prism Bootstrapper with autofac and module support 

## How to use it?
Todo...

## The prism bootstrapper with autofac ioc

	:::csharp

	 public class Bootstrapper : AutofacBootstrapper
	  {
		protected override void ConfigureContainer(ContainerBuilder builder)
		{
		  base.ConfigureContainer(builder);
		  builder.RegisterType<Shell>();

		  // register autofac module
		  builder.RegisterModule<MyModuleConfiguration>();
		}

		protected override void ConfigureModuleCatalog()
		{
		  base.ConfigureModuleCatalog();

		  // register prism module
		  Type typeNewsModule = typeof (MyModule);
		  ModuleCatalog.AddModule(new ModuleInfo(typeNewsModule.Name, typeNewsModule.AssemblyQualifiedName));
		}

		protected override DependencyObject CreateShell()
		{
		  return Container.Resolve<Shell>();
		}

		protected override void InitializeShell()
		{
		  base.InitializeShell();

		  Application.Current.MainWindow = (Shell)this.Shell;
		  Application.Current.MainWindow.Show();
		}
	}


## A simple configuration class that holds all needed type registrations for the module

	:::csharp

	public class MyModuleConfiguration : Module
	  {
		protected override void Load(ContainerBuilder builder)
		{
		  base.Load(builder);
		  
		  builder.RegisterType<MyModule>();
		  builder.RegisterType<MyViewModel>();
		  builder.RegisterType<MyView>().UsingConstructor(typeof(MyViewModel));
		}
	  }


## A very simple prism module (this code is now autofac independent)

	:::csharp
	public class MyModule : IModule
	  {
		private readonly IRegionManager _regionManager;

		public NewsModule(IRegionManager regionManager)
		{
		  _regionManager = regionManager;
		}

		public void Initialize()
		{
		  _regionManager.RegisterViewWithRegion("MainRegion", typeof(MyView));
		}
	}


Have fun!