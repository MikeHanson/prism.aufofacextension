﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Autofac;
using Microsoft.Practices.ServiceLocation;

namespace Prism.AutofacExtension
{
  internal class AutofacServiceLocator : IServiceLocator
  {
    private readonly IComponentContext _context;

    /// <summary>
    /// Initializes a new instance of the <see cref="AutofacServiceLocator"/> class.
    /// </summary>
    /// <param name="context">The context.</param>
    public AutofacServiceLocator(IComponentContext context)
    {
      _context = context;
    }

    public object GetService(Type serviceType)
    {
      return _context.Resolve(serviceType);
    }

    public object GetInstance(Type serviceType)
    {
      return _context.Resolve(serviceType);
    }

    public object GetInstance(Type serviceType, string key)
    {
      return _context.ResolveNamed(key, serviceType);
    }

    public IEnumerable<object> GetAllInstances(Type serviceType)
    {
      Type generic = typeof (IEnumerable<>);

      return (IEnumerable<object>) _context.Resolve(generic.MakeGenericType(serviceType));
    }

    public TService GetInstance<TService>()
    {
      return _context.Resolve<TService>();
    }

    public TService GetInstance<TService>(string key)
    {
      return _context.ResolveNamed<TService>(key);
    }

    public IEnumerable<TService> GetAllInstances<TService>()
    {
      return _context.Resolve<IEnumerable<TService>>();
    }
  }
}
