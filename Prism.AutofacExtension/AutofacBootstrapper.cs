﻿using System;
using Autofac;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Logging;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.Regions.Behaviors;
using Microsoft.Practices.ServiceLocation;

namespace Prism.AutofacExtension
{
  /// <summary>
  /// A autofac prism bootstrapper implementation class.
  /// </summary>
  public abstract class AutofacBootstrapper : Bootstrapper
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="AutofacBootstrapper"/> class.
    /// </summary>
    protected AutofacBootstrapper()
    {
      Builder = new ContainerBuilder();
    }

    private bool _useDefaultConfiguration = true;

    protected IContainer Container { get; private set; }

    private ContainerBuilder Builder { get; set; }

    public override void Run(bool runWithDefaultConfiguration)
    {
      _useDefaultConfiguration = runWithDefaultConfiguration;
      
      Logger = CreateLogger();
      if (Logger == null)
      {
        throw new InvalidOperationException(Resource.NullLoggerFacadeException);
      }

      this.Logger.Log(Resource.LoggerCreatedSuccessfully, Category.Debug, Priority.Low);

      this.Logger.Log(Resource.CreatingModuleCatalog, Category.Debug, Priority.Low);

      ModuleCatalog = CreateModuleCatalog();
      if (ModuleCatalog == null)
      {
        throw new InvalidOperationException(Resource.NullModuleCatalogException);
      }

      this.Logger.Log(Resource.ConfiguringModuleCatalog, Category.Debug, Priority.Low);
      ConfigureModuleCatalog();
      
      this.Logger.Log(Resource.ConfiguringAutofacContainer, Category.Debug, Priority.Low);
      ConfigureContainer(Builder);

      this.Logger.Log(Resource.CreatingAutofacContainer, Category.Debug, Priority.Low);
      Container = CreateContainer();

      if (Container == null)
      {
        throw new InvalidOperationException(Resource.NullAutofacContainerException);
      }
      
      this.Logger.Log(Resource.ConfiguringServiceLocatorSingleton, Category.Debug, Priority.Low);
      ConfigureServiceLocator();

      this.Logger.Log(Resource.ConfiguringRegionAdapters, Category.Debug, Priority.Low);
      ConfigureRegionAdapterMappings();

      this.Logger.Log(Resource.ConfiguringDefaultRegionBehaviors, Category.Debug, Priority.Low);
      ConfigureDefaultRegionBehaviors();

      this.Logger.Log(Resource.RegisteringFrameworkExceptionTypes, Category.Debug, Priority.Low);
      RegisterFrameworkExceptionTypes();

      this.Logger.Log(Resource.CreatingShell, Category.Debug, Priority.Low);
      Shell = CreateShell();

      if (Shell != null)
      {
        this.Logger.Log(Resource.SettingTheRegionManager, Category.Debug, Priority.Low);
        RegionManager.SetRegionManager(Shell, Container.Resolve<IRegionManager>());

        this.Logger.Log(Resource.UpdatingRegions, Category.Debug, Priority.Low);
        RegionManager.UpdateRegions();

        this.Logger.Log(Resource.InitializingShell, Category.Debug, Priority.Low);
        InitializeShell();
      }

      if (Container.IsRegistered<IModuleManager>())
      {
        this.Logger.Log(Resource.InitializingModules, Category.Debug, Priority.Low);
        InitializeModules();
      }
      
      this.Logger.Log(Resource.BootstrapperSequenceCompleted, Category.Debug, Priority.Low);
    }

    /// <summary>
    /// Registers the <see cref="T:System.Type"/>s of the Exceptions that are not considered
    /// root exceptions by the <see cref="T:Microsoft.Practices.Prism.ExceptionExtensions"/>.
    /// </summary>
    protected override void RegisterFrameworkExceptionTypes()
    {
      base.RegisterFrameworkExceptionTypes();

      ExceptionExtensions.RegisterFrameworkExceptionType(typeof(Autofac.Core.DependencyResolutionException));
    }

    /// <summary>
    /// Initializes the modules. May be overwritten in a derived class to use a custom Modules Catalog
    /// </summary>
    protected override void InitializeModules()
    {
      IModuleManager moduleManager = Container.Resolve<IModuleManager>();

      moduleManager.Run();
    }

    /// <summary>
    /// Creates the container.
    /// </summary>
    /// <returns></returns>
    protected virtual IContainer CreateContainer()
    {
      return Builder.Build();
    }

    /// <summary>
    /// Configures the container.
    /// </summary>
    /// <param name="builder">The builder.</param>
    protected virtual void ConfigureContainer(ContainerBuilder builder)
    {
      this.Logger.Log(Resource.AddingAutofacBootstrapperExtensionToContainer, Category.Debug, Priority.Low);
      builder.RegisterInstance<ILoggerFacade>(this.Logger);

      builder.RegisterInstance<IModuleCatalog>(this.ModuleCatalog);

      if (_useDefaultConfiguration)
      {
        RegisterTypeIfMissing(builder, typeof(IServiceLocator), typeof(AutofacServiceLocator), true);
        RegisterTypeIfMissing(builder, typeof(IModuleInitializer), typeof(ModuleInitializer), true);
        RegisterTypeIfMissing(builder, typeof(IModuleManager), typeof(ModuleManager), true);
        RegisterTypeIfMissing(builder, typeof(RegionAdapterMappings), typeof(RegionAdapterMappings), true);
        RegisterTypeIfMissing(builder, typeof(IRegionManager), typeof(RegionManager), true);

        RegisterTypeIfMissing(builder, typeof(SelectorRegionAdapter), typeof(SelectorRegionAdapter), true);
        RegisterTypeIfMissing(builder, typeof(ItemsControlRegionAdapter), typeof(ItemsControlRegionAdapter), true);
        RegisterTypeIfMissing(builder, typeof(ContentControlRegionAdapter), typeof(ContentControlRegionAdapter), true);

        RegisterTypeIfMissing(builder, typeof(DelayedRegionCreationBehavior), typeof(DelayedRegionCreationBehavior), false);
        RegisterTypeIfMissing(builder, typeof(BindRegionContextToDependencyObjectBehavior), typeof(BindRegionContextToDependencyObjectBehavior), false);
        RegisterTypeIfMissing(builder, typeof(AutoPopulateRegionBehavior), typeof(AutoPopulateRegionBehavior), false);
        RegisterTypeIfMissing(builder, typeof(RegionActiveAwareBehavior), typeof(RegionActiveAwareBehavior), false);
        RegisterTypeIfMissing(builder, typeof(SyncRegionContextWithHostBehavior), typeof(SyncRegionContextWithHostBehavior), false);
        RegisterTypeIfMissing(builder, typeof(RegionManagerRegistrationBehavior), typeof(RegionManagerRegistrationBehavior), false);
        RegisterTypeIfMissing(builder, typeof(RegionMemberLifetimeBehavior), typeof(RegionMemberLifetimeBehavior), false);
        RegisterTypeIfMissing(builder, typeof(ClearChildViewsRegionBehavior), typeof(ClearChildViewsRegionBehavior), false);

        RegisterTypeIfMissing(builder, typeof(IEventAggregator), typeof(EventAggregator), true);
        RegisterTypeIfMissing(builder, typeof(IRegionViewRegistry), typeof(RegionViewRegistry), true);
        RegisterTypeIfMissing(builder, typeof(IRegionBehaviorFactory), typeof(RegionBehaviorFactory), true);
        RegisterTypeIfMissing(builder, typeof(IRegionNavigationJournalEntry), typeof(RegionNavigationJournalEntry), false);
        RegisterTypeIfMissing(builder, typeof(IRegionNavigationJournal), typeof(RegionNavigationJournal), false);
        RegisterTypeIfMissing(builder, typeof(IRegionNavigationService), typeof(RegionNavigationService), false);
        RegisterTypeIfMissing(builder, typeof(IRegionNavigationContentLoader), typeof(RegionNavigationContentLoader), true);
      }
    }

    /// <summary>
    /// Registers the type if missing.
    /// </summary>
    /// <param name="builder">The builder.</param>
    /// <param name="fromType">From type.</param>
    /// <param name="toType">To type.</param>
    /// <param name="registerAsSingleton">if set to <c>true</c> [register as singleton].</param>
    private void RegisterTypeIfMissing(ContainerBuilder builder, Type fromType, Type toType, bool registerAsSingleton)
    {
      if (fromType == (Type)null)
      {
        throw new ArgumentNullException("fromType");
      }

      if (toType == (Type)null)
      {
        throw new ArgumentNullException("toType");
      }

      //// todo check errors if type already in container

      if (registerAsSingleton)
      {
        builder.RegisterType(toType).As(fromType).SingleInstance();
      }
      else
      {
        builder.RegisterType(toType).As(fromType);
      }
    }

    /// <summary>
    /// Configures the LocatorProvider for the <see cref="T:Microsoft.Practices.ServiceLocation.ServiceLocator"/>.
    /// </summary>
    protected override void ConfigureServiceLocator()
    {
      ServiceLocator.SetLocatorProvider(() => Container.Resolve<IServiceLocator>());
    }
  }
}