﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using Autofac;
using Autofac.Core;
using Microsoft.Practices.Prism;
using Microsoft.Practices.Prism.Logging;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using Moq;
using NUnit.Framework;

namespace Prism.AutofacExtension.Tests
{
  [TestFixture]
  public class AutofacBootstrapperTest
  {
    [Test]
    public void CanCreateConcreteBootstrapper()
    {
      new TestedBootstrapper();
    }

    [Test]
    public void ConfigureContainerAddsLoggerFacadeToContainer()
    {
      var bootstrapper = new TestedBootstrapper();
      bootstrapper.Run();

      var returnedCatalog = bootstrapper.BaseContainer.Resolve<ILoggerFacade>();
      Assert.IsNotNull(returnedCatalog);
    }

    [Test]
    public void ConfigureContainerAddsModuleCatalogToContainer()
    {
      var bootstrapper = new TestedBootstrapper();
      bootstrapper.Run();

      var returnedCatalog = bootstrapper.BaseContainer.Resolve<IModuleCatalog>();
      Assert.IsNotNull(returnedCatalog);
      Assert.IsTrue(returnedCatalog is ModuleCatalog);
    }

    [Test]
    public void ConfigureContainerAddsNavigationTargetHandlerToContainer()
    {
      var bootstrapper = new TestedBootstrapper();
      bootstrapper.Run();

      var actual1 = bootstrapper.BaseContainer.Resolve<IRegionNavigationContentLoader>();
      var actual2 = bootstrapper.BaseContainer.Resolve<IRegionNavigationContentLoader>();

      Assert.IsNotNull(actual1);
      Assert.IsNotNull(actual2);
      Assert.AreSame(actual1, actual2);
    }

    [Test]
    public void ConfigureContainerAddsRegionNavigationJournalEntryToContainer()
    {
      var bootstrapper = new TestedBootstrapper();
      bootstrapper.Run();

      var actual1 = bootstrapper.BaseContainer.Resolve<IRegionNavigationJournalEntry>();
      var actual2 = bootstrapper.BaseContainer.Resolve<IRegionNavigationJournalEntry>();

      Assert.IsNotNull(actual1);
      Assert.IsNotNull(actual2);
      Assert.AreNotSame(actual1, actual2);
    }

    [Test]
    public void ConfigureContainerAddsRegionNavigationJournalToContainer()
    {
      var bootstrapper = new TestedBootstrapper();
      bootstrapper.Run();

      var actual1 = bootstrapper.BaseContainer.Resolve<IRegionNavigationJournal>();
      var actual2 = bootstrapper.BaseContainer.Resolve<IRegionNavigationJournal>();

      Assert.IsNotNull(actual1);
      Assert.IsNotNull(actual2);
      Assert.AreNotSame(actual1, actual2);
    }

    [Test]
    public void ConfigureContainerAddsRegionNavigationServiceToContainer()
    {
      var bootstrapper = new TestedBootstrapper();
      bootstrapper.Run();

      var actual1 = bootstrapper.BaseContainer.Resolve<IRegionNavigationService>();
      var actual2 = bootstrapper.BaseContainer.Resolve<IRegionNavigationService>();

      Assert.IsNotNull(actual1);
      Assert.IsNotNull(actual2);
      Assert.AreNotSame(actual1, actual2);
    }

    [Test]
    public void ContainerDefaultsToNull()
    {
      var bootstrapper = new TestedBootstrapper();
      IContainer container = bootstrapper.BaseContainer;

      Assert.IsNull(container);
    }

    [Test]
    public void CreateContainerShouldInitializeContainer()
    {
      var bootstrapper = new TestedBootstrapper();

      IContainer container = bootstrapper.CallCreateContainer();

      Assert.IsNotNull(container);
      Assert.IsInstanceOf<IContainer>(container);
    }

    [Test]
    public void RegisterFrameworkExceptionTypesShouldRegisterActivationException()
    {
      var bootstrapper = new TestedBootstrapper();

      bootstrapper.CallRegisterFrameworkExceptionTypes();
      Assert.IsTrue(ExceptionExtensions.IsFrameworkExceptionRegistered(typeof (ActivationException)));
    }


    [Test]
    public void RegisterFrameworkExceptionTypesShouldRegisterResolutionFailedException()
    {
      var bootstrapper = new TestedBootstrapper();

      bootstrapper.CallRegisterFrameworkExceptionTypes();

      Assert.IsTrue(ExceptionExtensions.IsFrameworkExceptionRegistered(typeof (DependencyResolutionException)));
    }


    [Test]
    public void When_resolve_servicelocator_should_getallinstances_return_correct_count()
    {
      var bootstrapper = new TestedBootstrapper();
      bootstrapper.Run();

      IServiceLocator serviceLocator = bootstrapper.BaseContainer.Resolve<IServiceLocator>();

      IEnumerable<object> allInstances = serviceLocator.GetAllInstances(typeof(TestObject));

      Assert.AreEqual(2, allInstances.Count());

      IEnumerable<TestObject> objects = (IEnumerable<TestObject>) serviceLocator.GetAllInstances(typeof(TestObject));
      Assert.AreEqual(2, objects.Count());
      Assert.IsTrue(bootstrapper.ConfigureServiceLocatorCalled);
    }

    private class TestObject
    {
      
    }

    private class TestedBootstrapper : AutofacBootstrapper
    {
      public readonly List<string> MethodCalls = new List<string>();
      public readonly DependencyObject ShellObject = new UserControl();
      public bool ConfigureContainerCalled;
      public bool ConfigureDefaultRegionBehaviorsCalled;
      public bool ConfigureModuleCatalogCalled;
      public bool ConfigureRegionAdapterMappingsCalled;
      public bool ConfigureServiceLocatorCalled;
      public bool CreateContainerCalled;
      public bool CreateLoggerCalled;
      public bool CreateModuleCatalogCalled;
      public bool CreateShellCalled;
      public RegionAdapterMappings DefaultRegionAdapterMappings;
      public bool InitializeModulesCalled;
      public bool InitializeShellCalled;

      public DependencyObject BaseShell
      {
        get { return base.Shell; }
      }

      public IContainer BaseContainer
      {
        get { return base.Container; }
      }

      public ILoggerFacade BaseLogger
      {
        get { return base.Logger; }
      }

      public IContainer CallCreateContainer()
      {
        return CreateContainer();
      }

      protected override IContainer CreateContainer()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        CreateContainerCalled = true;
        return base.CreateContainer();
      }

      protected override void ConfigureContainer(ContainerBuilder builder)
      {
        builder.RegisterType<TestObject>();
        builder.RegisterType<TestObject>();
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        ConfigureContainerCalled = true;
        base.ConfigureContainer(builder);
      }

      protected override ILoggerFacade CreateLogger()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        CreateLoggerCalled = true;
        return new Mock<ILoggerFacade>().Object;
      }

      protected override DependencyObject CreateShell()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        CreateShellCalled = true;
        return ShellObject;
      }

      protected override void ConfigureServiceLocator()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        ConfigureServiceLocatorCalled = true;
        base.ConfigureServiceLocator();
      }

      protected override IModuleCatalog CreateModuleCatalog()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        CreateModuleCatalogCalled = true;
        return base.CreateModuleCatalog();
      }

      protected override void ConfigureModuleCatalog()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        ConfigureModuleCatalogCalled = true;
        base.ConfigureModuleCatalog();
      }

      protected override void InitializeShell()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        InitializeShellCalled = true;
        // no op
      }

      protected override void InitializeModules()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        InitializeModulesCalled = true;
        base.InitializeModules();
      }

      protected override IRegionBehaviorFactory ConfigureDefaultRegionBehaviors()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        ConfigureDefaultRegionBehaviorsCalled = true;
        return base.ConfigureDefaultRegionBehaviors();
      }

      protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        ConfigureRegionAdapterMappingsCalled = true;
        RegionAdapterMappings regionAdapterMappings = base.ConfigureRegionAdapterMappings();

        DefaultRegionAdapterMappings = regionAdapterMappings;

        return regionAdapterMappings;
      }

      protected override void RegisterFrameworkExceptionTypes()
      {
        MethodCalls.Add(MethodBase.GetCurrentMethod().Name);
        base.RegisterFrameworkExceptionTypes();
      }

      public void CallRegisterFrameworkExceptionTypes()
      {
        base.RegisterFrameworkExceptionTypes();
      }
    }
  }
}